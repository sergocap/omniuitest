FROM node:12.16.1-alpine

RUN apk update && apk add --no-cache bash build-base git

WORKDIR /app

COPY . .

RUN yarn install
